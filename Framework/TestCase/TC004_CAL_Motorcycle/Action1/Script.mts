﻿'iRowCount = Datatable.getSheet("TC003 [TC003_CAL_Conditinal]").getRowCount
''คำนวณสินเชื่อแบบระบุเงื่อนไข
URL = Trim((DataTable("URL","TC004 [TC004_CAL_Motorcycle]")))

Product_type = Trim((DataTable("Product_type","TC004 [TC004_CAL_Motorcycle]")))
Expected_Product_type = Trim((DataTable("Product_type","TC004 [TC004_CAL_Motorcycle]")))
Car_price = Trim((DataTable("Car_price","TC004 [TC004_CAL_Motorcycle]")))
Expected_Car_price = Trim((DataTable("Expected_Car_price","TC004 [TC004_CAL_Motorcycle]")))
Down_Payment = Trim((DataTable("Down_Payment","TC004 [TC004_CAL_Motorcycle]")))
Expected_Down_Payment = Trim((DataTable("Expected_Down_Payment","TC004 [TC004_CAL_Motorcycle]")))
Installments_Period = Trim((DataTable("Installments_Period","TC004 [TC004_CAL_Motorcycle]")))
Expected_Installments_Period = Trim((DataTable("Expected_Installments_Period","TC004 [TC004_CAL_Motorcycle]")))
Interest_rate_percent = Trim((DataTable("Interest_rate_percent","TC004 [TC004_CAL_Motorcycle]")))
Expected_Interest_rate_percent = Trim((DataTable("Expected_Interest_rate_percent","TC004 [TC004_CAL_Motorcycle]")))
Expected_Buy_loan = Trim((DataTable("Expected_Buy_loan","TC004 [TC004_CAL_Motorcycle]")))
Expected_loan_per_month = Trim((DataTable("Expected_loan_per_month","TC004 [TC004_CAL_Motorcycle]")))


'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("หน้าหลัก | กรุงศรี ออโต้").WebElement("สมัครสินเชื่อรถยนต์").Click @@ script infofile_;_ZIP::ssf1.xml_;_
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("หน้าหลัก | กรุงศรี ออโต้").Link("คำนวณสินเชื่อเบื้องต้น").Click @@ script infofile_;_ZIP::ssf2.xml_;_
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebElement("คำนวณสินเชื่อ").Click @@ script infofile_;_ZIP::ssf3.xml_;_
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebButton("กรุงศรี นิวคาร์").Click
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebList("กรุงศรี นิวคาร์กรุงศรี").Select "กรุงศรี มอเตอร์ไซค์"
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebEdit("car_price1").Set "60,000"
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebEdit("down_payment1").Set "10,000"
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebButton("48").Click
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebList("182430364248").Select "48"
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebEdit("interest_rate_percent1").Set "3"
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebElement("%").Click @@ script infofile_;_ZIP::ssf11.xml_;_
''Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebButton("คำนวณ").Click @@ script infofile_;_ZIP::ssf12.xml_;_
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebElement("ยอดจัดเช่าซื้อ บาท ค่างวดต่อเด").Click @@ script infofile_;_ZIP::ssf13.xml_;_
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebEdit("buy_loan1").GetROProperty("value")
'Browser("หน้าหลัก | กรุงศรี ออโต้").Page("คำนวณสินเชื่อรถยนต์ |").WebEdit("loan_per_month1").GetROProperty("value")



Call FW_OpenWebBrowser("เปิดเวปกรุงศรี กรุงศรี ออโต้", URL,"CHROME")
Call FW_WebClickLinkAndVerify_2_PageElementResult("คลิกปุ่ม คำนวณสินเชื่อเบื้องต้น", "หน้าหลัก | กรุงศรี ออโต้", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "คำนวณสินเชื่อเบื้องต้น", "คำนวณสินเชื่อ", "คำนวณสินเชื่อ") @@ script infofile_;_ZIP::ssf4.xml_;_
Call FW_WebListAndVerifyResult("เลือกประเภทสินเชื่อ", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "กรุงศรี นิวคาร์กรุงศรี", Product_type, Expected_Product_type)
Call FW_WebEditAndVerifyResult("กรอกราคารถ (ไม่รวม VAT)", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "car_price1", Car_price, Expected_Car_price)
Call FW_WebEditAndVerifyResult("กรอกเงินดาวน์", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "down_payment1", Down_Payment, Expected_Down_Payment)
Call FW_WebListAndVerifyResult("เลือก ระยะเวลาผ่อนชำระ", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "182430364248", Installments_Period, Expected_Installments_Period)
Call FW_WebEditAndVerifyResult("กรอกดอกเบี้ย", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "interest_rate_percent1", Interest_rate_percent, Expected_Interest_rate_percent)
CAll FW_WebButtonAndVerifyEdit2Result("กดปุ่มคำนวณสินเชื่อ", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "คำนวณ", "buy_loan1", "loan_per_month1", Expected_Buy_loan, Expected_loan_per_month)
CAll FW_WebButtonAndVerifyEdit2Result("ตรวจสอบยอดจัดเช่าซื้อ", "หน้าหลัก | กรุงศรี ออโต้", "คำนวณสินเชื่อรถยนต์ |", "คำนวณ", "buy_loan1", "loan_per_month1", Expected_Buy_loan, Expected_loan_per_month)
Call FW_CloseWebBrowserChrome("ปิดเว็บกรุงศรีคำนวนสินเชื่อ")
